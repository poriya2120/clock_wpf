﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

namespace WpfApplication18
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Timer t = new Timer();
        public MainWindow()
        {
            InitializeComponent();
            t.Elapsed += T_Elapsed;
            t.Start();
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
         Dispatcher.Invoke((Action)(()=>
             {
                 seconrot.Angle = DateTime.Now.Second * 6;
                 minrot.Angle = DateTime.Now.Minute * 6;
                 hourrot.Angle = DateTime.Now.Hour * 30;

             }));
        }
    }
}
